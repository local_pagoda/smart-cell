<?php namespace Bijay\Banners\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableCreateBijayBannersImages extends Migration
{
    public function up()
    {
        Schema::create('bijay_banners_images', function($table)
        {
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->string('title')->nullable();
            $table->string('sub_title')->nullable();
            $table->string('description')->nullable();
        });
    }
    
    public function down()
    {
        Schema::dropIfExists('bijay_banners_images');
    }
}
