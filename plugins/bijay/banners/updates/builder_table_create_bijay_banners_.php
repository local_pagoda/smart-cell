<?php namespace Bijay\Banners\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableCreateBijayBanners extends Migration
{
    public function up()
    {
        Schema::create('bijay_banners_', function($table)
        {
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->string('name', 255);
        });
    }
    
    public function down()
    {
        Schema::dropIfExists('bijay_banners_');
    }
}
