<?php

/* C:\xampp\htdocs\smart-cell/themes/smart/pages/home.htm */
class __TwigTemplate_3ea5d4a5266b16b13ec78a57f38a341fa4f03f80266541c598978ebf12c5eccd extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo "<section class=\"co-banner-section\">
    <div class=\"home-banner-slider\">
    \t";
        // line 3
        $context["record"] = twig_get_attribute($this->env, $this->getSourceContext(), ($context["builderDetails"] ?? null), "record", array());
        // line 4
        echo "\t\t";
        $context["displayColumn"] = twig_get_attribute($this->env, $this->getSourceContext(), ($context["builderDetails"] ?? null), "displayColumn", array());
        // line 5
        echo "\t\t";
        $context["notFoundMessage"] = twig_get_attribute($this->env, $this->getSourceContext(), ($context["builderDetails"] ?? null), "notFoundMessage", array());
        // line 6
        echo "\t\t
\t\t";
        // line 7
        if (($context["record"] ?? null)) {
            // line 8
            echo "\t\t    
\t\t    ";
            // line 9
            $context['_parent'] = $context;
            $context['_seq'] = twig_ensure_traversable(twig_get_attribute($this->env, $this->getSourceContext(), ($context["record"] ?? null), "images", array()));
            foreach ($context['_seq'] as $context["_key"] => $context["image"]) {
                // line 10
                echo "\t\t    \t
\t\t    \t<div class=\"banner-item\">
\t\t            <img src=\"";
                // line 12
                echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->getSourceContext(), $context["image"], "path", array()), "html", null, true);
                echo "\" class=\"co-background-image\">
\t\t            <div class=\"banner-text co-pack-block\">
\t\t                <div class=\"title\">
\t\t                    <h2>";
                // line 15
                echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->getSourceContext(), $context["image"], "title", array()), "html", null, true);
                echo "</h2>
\t\t                </div>
\t\t                <div class=\"sub-text\">
\t\t                    <p>Calls to any network on first recharge pack</p>
\t\t                </div>
\t\t
\t\t                <div class=\"activation-text\">
\t\t                    <ul>
\t\t                        <li>Dial *141* to subscribe</li>
\t\t                        <li>Call: 4343/9802049341</li>
\t\t                        <li>Online Support</li>
\t\t                    </ul>
\t\t                </div>
\t\t            </div>
\t\t        </div>
\t\t\t";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['image'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 31
            echo "\t\t    
\t\t";
        } else {
            // line 33
            echo "\t\t    ";
            echo twig_escape_filter($this->env, ($context["notFoundMessage"] ?? null), "html", null, true);
            echo "
\t\t";
        }
        // line 35
        echo "        
        
    </div>
</section>



<section class=\"home-packs-section\">
    <div class=\"custom-container\">
        <div class=\"home-packs-slider\">
            <div class=\"co-pack-block padding\">
                <div class=\"co-pack-inside\">
                    <div class=\"title\">
                        <h3>Double Data</h3>
                        <h3>Double Mazaa</h3>
                    </div>
                    <div class=\"sub-text\">
                        <p>Enjoy two times the data at same affordable price</p>
                    </div>

                    <div class=\"activation-text\">
                        <ul>
                            <li>Dial *141* to subscribe</li>
                        </ul>
                    </div>
                    <a href=\"#\" class=\"smart-btn btn-margin\">Know more</a>
                </div>
            </div>
            <div class=\"co-pack-block padding\">
                <div class=\"co-pack-inside\">
                    <div class=\"title\">
                        <h3>Double Data</h3>
                        <h3>Double Mazaa</h3>
                    </div>
                    <div class=\"sub-text\">
                        <p>Enjoy two times the data at same affordable price</p>
                    </div>

                    <div class=\"activation-text\">
                        <ul>
                            <li>Dial *141* to subscribe</li>
                        </ul>
                    </div>
                    <button type=\"submit\" class=\"smart-btn btn-margin\">Know more</button>
                </div>
            </div>
            <div class=\"co-pack-block padding\">
                <div class=\"co-pack-inside\">
                    <div class=\"title\">
                        <h3>Double Data</h3>
                        <h3>Double Mazaa</h3>
                    </div>
                    <div class=\"sub-text\">
                        <p>Enjoy two times the data at same affordable price</p>
                    </div>

                    <div class=\"activation-text\">
                        <ul>
                            <li>Dial *141* to subscribe</li>
                        </ul>
                    </div>
                    <input type=\"submit\" class=\"smart-btn btn-margin\" value=\"Know More\">
                </div>
            </div>
            <div class=\"co-pack-block padding\">
                <div class=\"co-pack-inside\">
                    <div class=\"title\">
                        <h3>Double Data</h3>
                        <h3>Double Mazaa</h3>
                    </div>
                    <div class=\"sub-text\">
                        <p>Enjoy two times the data at same affordable price</p>
                    </div>

                    <div class=\"activation-text\">
                        <ul>
                            <li>Dial *141* to subscribe</li>
                        </ul>
                    </div>
                    <input type=\"submit\" class=\"smart-btn btn-margin\" value=\"Know More\">
                </div>
            </div>
            <div class=\"co-pack-block padding\">
                <div class=\"co-pack-inside\">
                    <div class=\"title\">
                        <h3>Double Data</h3>
                        <h3>Double Mazaa</h3>
                    </div>
                    <div class=\"sub-text\">
                        <p>Enjoy two times the data at same affordable price</p>
                    </div>

                    <div class=\"activation-text\">
                        <ul>
                            <li>Dial *141* to subscribe</li>
                        </ul>
                    </div>
                    <input type=\"submit\" class=\"smart-btn btn-margin\" value=\"Know More\">
                </div>
            </div>
            <div class=\"co-pack-block padding\">
                <div class=\"co-pack-inside\">
                    <div class=\"title\">
                        <h3>Double Data</h3>
                        <h3>Double Mazaa</h3>
                    </div>
                    <div class=\"sub-text\">
                        <p>Enjoy two times the data at same affordable price</p>
                    </div>

                    <div class=\"activation-text\">
                        <ul>
                            <li>Dial *141* to subscribe</li>
                        </ul>
                    </div>
                    <input type=\"submit\" class=\"smart-btn btn-margin\" value=\"Know More\">
                </div>
            </div>
        </div>
    </div>
</section>



<section class=\"home-three-box-section\">
    <div class=\"custom-container small-width-layout add-margin-bottom\">
        <div class=\"image-container\">
        \t";
        // line 162
        $context["record"] = twig_get_attribute($this->env, $this->getSourceContext(), ($context["builderDetails2"] ?? null), "record", array());
        // line 163
        echo "\t\t\t";
        $context["displayColumn"] = twig_get_attribute($this->env, $this->getSourceContext(), ($context["builderDetails2"] ?? null), "displayColumn", array());
        // line 164
        echo "\t\t\t";
        $context["notFoundMessage"] = twig_get_attribute($this->env, $this->getSourceContext(), ($context["builderDetails2"] ?? null), "notFoundMessage", array());
        // line 165
        echo "\t\t\t
\t\t\t";
        // line 166
        if (($context["record"] ?? null)) {
            // line 167
            echo "\t\t\t    ";
            $context['_parent'] = $context;
            $context['_seq'] = twig_ensure_traversable(twig_get_attribute($this->env, $this->getSourceContext(), ($context["record"] ?? null), "images", array()));
            foreach ($context['_seq'] as $context["_key"] => $context["image"]) {
                // line 168
                echo "\t\t\t    <img src=\"";
                echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->getSourceContext(), $context["image"], "path", array()), "html", null, true);
                echo "\" class=\"co-background-image\">
\t\t\t    ";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['image'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 170
            echo "\t\t\t";
        } else {
            // line 171
            echo "\t\t\t    <img src=\"";
            echo $this->env->getExtension('Cms\Twig\Extension')->themeFilter("assets/images/background/plan-background.png");
            echo "\" class=\"co-background-image\">
\t\t\t";
        }
        // line 173
        echo "            
            <div class=\"three-box-bottom-fix\">
                <div class=\"three-box-bottom-fix-inner\">
                    <div class=\"three-box-header\">
                        <h2>";
        // line 177
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->getSourceContext(), twig_get_attribute($this->env, $this->getSourceContext(), ($context["record"] ?? null), "images", array()), "title", array()), "html", null, true);
        echo "</h2>
                    </div>
                    <div class=\"three-box-item\" style=\"background-color: #f4e3ca\">
                        <div class=\"price\">
                            <h1>37<sup>Rs</sup></h1>
                        </div>
                        <div class=\"detail\">
                            <p>Talk Unlimited</p>
                            <p>7 days</p>
                        </div>
                    </div>
                    <div class=\"three-box-item\" style=\"background-color: #b6dbe1\">
                        <div class=\"price\">
                            <h1>37<sup>Rs</sup></h1>
                        </div>
                        <div class=\"detail\">
                            <p>Talk Unlimited</p>
                            <p>7 days</p>
                        </div>
                    </div>
                    <div class=\"three-box-item\" style=\"background-color: #d5ccc8\">
                        <div class=\"price\">
                            <h1>37<sup>Rs</sup></h1>
                        </div>
                        <div class=\"detail\">
                            <p>Talk Unlimited</p>
                            <p>7 days</p>
                        </div>
                    </div>
                </div>
            </div>

        </div>
    </div>
    <div class=\"custom-container\">
        <div class=\"text-container smallest-width-layout\">
            <h2>With smart cell connect internationally with call and text plans that fit your budget. </h2>
            <a href=\"#\" class=\"smart-btn btn-margin double\">Know more</a>
        </div>
    </div>
</section>";
    }

    public function getTemplateName()
    {
        return "C:\\xampp\\htdocs\\smart-cell/themes/smart/pages/home.htm";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  256 => 177,  250 => 173,  244 => 171,  241 => 170,  232 => 168,  227 => 167,  225 => 166,  222 => 165,  219 => 164,  216 => 163,  214 => 162,  85 => 35,  79 => 33,  75 => 31,  53 => 15,  47 => 12,  43 => 10,  39 => 9,  36 => 8,  34 => 7,  31 => 6,  28 => 5,  25 => 4,  23 => 3,  19 => 1,);
    }

    public function getSourceContext()
    {
        return new Twig_Source("<section class=\"co-banner-section\">
    <div class=\"home-banner-slider\">
    \t{% set record = builderDetails.record %}
\t\t{% set displayColumn = builderDetails.displayColumn %}
\t\t{% set notFoundMessage = builderDetails.notFoundMessage %}
\t\t
\t\t{% if record %}
\t\t    
\t\t    {% for image in record.images%}
\t\t    \t
\t\t    \t<div class=\"banner-item\">
\t\t            <img src=\"{{ image.path }}\" class=\"co-background-image\">
\t\t            <div class=\"banner-text co-pack-block\">
\t\t                <div class=\"title\">
\t\t                    <h2>{{ image.title }}</h2>
\t\t                </div>
\t\t                <div class=\"sub-text\">
\t\t                    <p>Calls to any network on first recharge pack</p>
\t\t                </div>
\t\t
\t\t                <div class=\"activation-text\">
\t\t                    <ul>
\t\t                        <li>Dial *141* to subscribe</li>
\t\t                        <li>Call: 4343/9802049341</li>
\t\t                        <li>Online Support</li>
\t\t                    </ul>
\t\t                </div>
\t\t            </div>
\t\t        </div>
\t\t\t{% endfor %}
\t\t    
\t\t{% else %}
\t\t    {{ notFoundMessage }}
\t\t{% endif %}
        
        
    </div>
</section>



<section class=\"home-packs-section\">
    <div class=\"custom-container\">
        <div class=\"home-packs-slider\">
            <div class=\"co-pack-block padding\">
                <div class=\"co-pack-inside\">
                    <div class=\"title\">
                        <h3>Double Data</h3>
                        <h3>Double Mazaa</h3>
                    </div>
                    <div class=\"sub-text\">
                        <p>Enjoy two times the data at same affordable price</p>
                    </div>

                    <div class=\"activation-text\">
                        <ul>
                            <li>Dial *141* to subscribe</li>
                        </ul>
                    </div>
                    <a href=\"#\" class=\"smart-btn btn-margin\">Know more</a>
                </div>
            </div>
            <div class=\"co-pack-block padding\">
                <div class=\"co-pack-inside\">
                    <div class=\"title\">
                        <h3>Double Data</h3>
                        <h3>Double Mazaa</h3>
                    </div>
                    <div class=\"sub-text\">
                        <p>Enjoy two times the data at same affordable price</p>
                    </div>

                    <div class=\"activation-text\">
                        <ul>
                            <li>Dial *141* to subscribe</li>
                        </ul>
                    </div>
                    <button type=\"submit\" class=\"smart-btn btn-margin\">Know more</button>
                </div>
            </div>
            <div class=\"co-pack-block padding\">
                <div class=\"co-pack-inside\">
                    <div class=\"title\">
                        <h3>Double Data</h3>
                        <h3>Double Mazaa</h3>
                    </div>
                    <div class=\"sub-text\">
                        <p>Enjoy two times the data at same affordable price</p>
                    </div>

                    <div class=\"activation-text\">
                        <ul>
                            <li>Dial *141* to subscribe</li>
                        </ul>
                    </div>
                    <input type=\"submit\" class=\"smart-btn btn-margin\" value=\"Know More\">
                </div>
            </div>
            <div class=\"co-pack-block padding\">
                <div class=\"co-pack-inside\">
                    <div class=\"title\">
                        <h3>Double Data</h3>
                        <h3>Double Mazaa</h3>
                    </div>
                    <div class=\"sub-text\">
                        <p>Enjoy two times the data at same affordable price</p>
                    </div>

                    <div class=\"activation-text\">
                        <ul>
                            <li>Dial *141* to subscribe</li>
                        </ul>
                    </div>
                    <input type=\"submit\" class=\"smart-btn btn-margin\" value=\"Know More\">
                </div>
            </div>
            <div class=\"co-pack-block padding\">
                <div class=\"co-pack-inside\">
                    <div class=\"title\">
                        <h3>Double Data</h3>
                        <h3>Double Mazaa</h3>
                    </div>
                    <div class=\"sub-text\">
                        <p>Enjoy two times the data at same affordable price</p>
                    </div>

                    <div class=\"activation-text\">
                        <ul>
                            <li>Dial *141* to subscribe</li>
                        </ul>
                    </div>
                    <input type=\"submit\" class=\"smart-btn btn-margin\" value=\"Know More\">
                </div>
            </div>
            <div class=\"co-pack-block padding\">
                <div class=\"co-pack-inside\">
                    <div class=\"title\">
                        <h3>Double Data</h3>
                        <h3>Double Mazaa</h3>
                    </div>
                    <div class=\"sub-text\">
                        <p>Enjoy two times the data at same affordable price</p>
                    </div>

                    <div class=\"activation-text\">
                        <ul>
                            <li>Dial *141* to subscribe</li>
                        </ul>
                    </div>
                    <input type=\"submit\" class=\"smart-btn btn-margin\" value=\"Know More\">
                </div>
            </div>
        </div>
    </div>
</section>



<section class=\"home-three-box-section\">
    <div class=\"custom-container small-width-layout add-margin-bottom\">
        <div class=\"image-container\">
        \t{% set record = builderDetails2.record %}
\t\t\t{% set displayColumn = builderDetails2.displayColumn %}
\t\t\t{% set notFoundMessage = builderDetails2.notFoundMessage %}
\t\t\t
\t\t\t{% if record %}
\t\t\t    {% for image in record.images%}
\t\t\t    <img src=\"{{ image.path }}\" class=\"co-background-image\">
\t\t\t    {% endfor %}
\t\t\t{% else %}
\t\t\t    <img src=\"{{ 'assets/images/background/plan-background.png'|theme}}\" class=\"co-background-image\">
\t\t\t{% endif %}
            
            <div class=\"three-box-bottom-fix\">
                <div class=\"three-box-bottom-fix-inner\">
                    <div class=\"three-box-header\">
                        <h2>{{ record.images.title }}</h2>
                    </div>
                    <div class=\"three-box-item\" style=\"background-color: #f4e3ca\">
                        <div class=\"price\">
                            <h1>37<sup>Rs</sup></h1>
                        </div>
                        <div class=\"detail\">
                            <p>Talk Unlimited</p>
                            <p>7 days</p>
                        </div>
                    </div>
                    <div class=\"three-box-item\" style=\"background-color: #b6dbe1\">
                        <div class=\"price\">
                            <h1>37<sup>Rs</sup></h1>
                        </div>
                        <div class=\"detail\">
                            <p>Talk Unlimited</p>
                            <p>7 days</p>
                        </div>
                    </div>
                    <div class=\"three-box-item\" style=\"background-color: #d5ccc8\">
                        <div class=\"price\">
                            <h1>37<sup>Rs</sup></h1>
                        </div>
                        <div class=\"detail\">
                            <p>Talk Unlimited</p>
                            <p>7 days</p>
                        </div>
                    </div>
                </div>
            </div>

        </div>
    </div>
    <div class=\"custom-container\">
        <div class=\"text-container smallest-width-layout\">
            <h2>With smart cell connect internationally with call and text plans that fit your budget. </h2>
            <a href=\"#\" class=\"smart-btn btn-margin double\">Know more</a>
        </div>
    </div>
</section>", "C:\\xampp\\htdocs\\smart-cell/themes/smart/pages/home.htm", "");
    }
}
