<?php

/* C:\xampp\htdocs\smart-cell/themes/smart/partials/site/header.htm */
class __TwigTemplate_32521511ef6e0ca26b8b61305e8602776778d1f7f22f553783675efcebce8b28 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo "<!-- Nav -->
<!-- <nav id=\"layout-nav\" class=\"navbar navbar-inverse navbar-fixed-top navbar-autohide\" role=\"navigation\">
    <div class=\"container\">
        <div class=\"navbar-header\">
            <button type=\"button\" class=\"navbar-toggle\" data-toggle=\"collapse\" data-target=\".navbar-main-collapse\">
                <span class=\"sr-only\">Toggle navigation</span>
                <span class=\"icon-bar\"></span>
                <span class=\"icon-bar\"></span>
                <span class=\"icon-bar\"></span>
            </button>
            <a class=\"navbar-brand\" href=\"";
        // line 11
        echo $this->env->getExtension('Cms\Twig\Extension')->pageFilter("home");
        echo "\">October Demo</a>
        </div>
        <div class=\"collapse navbar-collapse navbar-main-collapse\">
            <ul class=\"nav navbar-nav\">
                <li class=\"separator hidden-xs\"></li>
                <li class=\"";
        // line 16
        if ((twig_get_attribute($this->env, $this->getSourceContext(), twig_get_attribute($this->env, $this->getSourceContext(), ($context["this"] ?? null), "page", array()), "id", array()) == "home")) {
            echo "active";
        }
        echo "\"><a href=\"";
        echo $this->env->getExtension('Cms\Twig\Extension')->pageFilter("home");
        echo "\">Basic concepts</a></li>
                <li class=\"";
        // line 17
        if ((twig_get_attribute($this->env, $this->getSourceContext(), twig_get_attribute($this->env, $this->getSourceContext(), ($context["this"] ?? null), "page", array()), "id", array()) == "ajax")) {
            echo "active";
        }
        echo "\"><a href=\"";
        echo $this->env->getExtension('Cms\Twig\Extension')->pageFilter("ajax");
        echo "\">AJAX framework</a></li>
                <li class=\"";
        // line 18
        if ((twig_get_attribute($this->env, $this->getSourceContext(), twig_get_attribute($this->env, $this->getSourceContext(), ($context["this"] ?? null), "page", array()), "id", array()) == "plugins")) {
            echo "active";
        }
        echo "\"><a href=\"";
        echo $this->env->getExtension('Cms\Twig\Extension')->pageFilter("plugins");
        echo "\">Plugin components</a></li>
            </ul>
        </div>
    </div>
</nav> -->

<div class=\"custom-container small-width-layout\">
    <div class=\"logo-nav-wrapper\">
        <div class=\"logo-container\">
            <a href=\"";
        // line 27
        echo $this->env->getExtension('Cms\Twig\Extension')->pageFilter("home");
        echo "\">
                <img src=\"";
        // line 28
        echo $this->env->getExtension('Cms\Twig\Extension')->themeFilter("assets/img/site_logo.png");
        echo "\">

            </a>
        </div>
        <div class=\"nav-container\">
            <nav>
                <ul>
                    <li class=\"";
        // line 35
        if ((twig_get_attribute($this->env, $this->getSourceContext(), twig_get_attribute($this->env, $this->getSourceContext(), ($context["this"] ?? null), "page", array()), "id", array()) == "4G")) {
            echo "active";
        }
        echo "\"><a href=\"4G.php\">4G & Internet</a>
<!--                                    <ul>
                            <li><a href=\"#\">Jumbo Data Pack</a></li>
                            <li><a href=\"#\">Double Data</a></li>
                            <li><a href=\"#\">First recharge pack</a></li>
                        </ul>-->
                    </li>
                    <!-- <li><a href=\"product.php\">Products</a></li> -->
                    <li class=\"";
        // line 43
        if ((twig_get_attribute($this->env, $this->getSourceContext(), twig_get_attribute($this->env, $this->getSourceContext(), ($context["this"] ?? null), "page", array()), "id", array()) == "product")) {
            echo "active";
        }
        echo "\"><a href=\"";
        echo $this->env->getExtension('Cms\Twig\Extension')->pageFilter("product");
        echo "\">Products</a></li>
                    <li class=\"";
        // line 44
        if ((twig_get_attribute($this->env, $this->getSourceContext(), twig_get_attribute($this->env, $this->getSourceContext(), ($context["this"] ?? null), "page", array()), "id", array()) == "offer")) {
            echo "active";
        }
        echo "\"><a href=\"";
        echo $this->env->getExtension('Cms\Twig\Extension')->pageFilter("offer");
        echo "\">Offer</a></li>
                    <li class=\"";
        // line 45
        if ((twig_get_attribute($this->env, $this->getSourceContext(), twig_get_attribute($this->env, $this->getSourceContext(), ($context["this"] ?? null), "page", array()), "id", array()) == "international_call")) {
            echo "active";
        }
        echo "\"><a href=\"";
        echo $this->env->getExtension('Cms\Twig\Extension')->pageFilter("international_call");
        echo "\">International Call</a></li>
                    <li class=\"";
        // line 46
        if ((twig_get_attribute($this->env, $this->getSourceContext(), twig_get_attribute($this->env, $this->getSourceContext(), ($context["this"] ?? null), "page", array()), "id", array()) == "services")) {
            echo "active";
        }
        echo "\"><a href=\"";
        echo $this->env->getExtension('Cms\Twig\Extension')->pageFilter("services");
        echo "\">Services</a></li>
                    <!-- <li><a href=\"#\">Offer</a></li>
                    <li><a href=\"#\">International Call</a></li>
                    <li><a href=\"#\">Services</a></li> -->
                </ul>
            </nav>
        </div>
    </div>
</div>";
    }

    public function getTemplateName()
    {
        return "C:\\xampp\\htdocs\\smart-cell/themes/smart/partials/site/header.htm";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  122 => 46,  114 => 45,  106 => 44,  98 => 43,  85 => 35,  75 => 28,  71 => 27,  55 => 18,  47 => 17,  39 => 16,  31 => 11,  19 => 1,);
    }

    public function getSourceContext()
    {
        return new Twig_Source("<!-- Nav -->
<!-- <nav id=\"layout-nav\" class=\"navbar navbar-inverse navbar-fixed-top navbar-autohide\" role=\"navigation\">
    <div class=\"container\">
        <div class=\"navbar-header\">
            <button type=\"button\" class=\"navbar-toggle\" data-toggle=\"collapse\" data-target=\".navbar-main-collapse\">
                <span class=\"sr-only\">Toggle navigation</span>
                <span class=\"icon-bar\"></span>
                <span class=\"icon-bar\"></span>
                <span class=\"icon-bar\"></span>
            </button>
            <a class=\"navbar-brand\" href=\"{{ 'home'|page }}\">October Demo</a>
        </div>
        <div class=\"collapse navbar-collapse navbar-main-collapse\">
            <ul class=\"nav navbar-nav\">
                <li class=\"separator hidden-xs\"></li>
                <li class=\"{% if this.page.id == 'home' %}active{% endif %}\"><a href=\"{{ 'home'|page }}\">Basic concepts</a></li>
                <li class=\"{% if this.page.id == 'ajax' %}active{% endif %}\"><a href=\"{{ 'ajax'|page }}\">AJAX framework</a></li>
                <li class=\"{% if this.page.id == 'plugins' %}active{% endif %}\"><a href=\"{{ 'plugins'|page }}\">Plugin components</a></li>
            </ul>
        </div>
    </div>
</nav> -->

<div class=\"custom-container small-width-layout\">
    <div class=\"logo-nav-wrapper\">
        <div class=\"logo-container\">
            <a href=\"{{ 'home'|page }}\">
                <img src=\"{{ 'assets/img/site_logo.png'|theme }}\">

            </a>
        </div>
        <div class=\"nav-container\">
            <nav>
                <ul>
                    <li class=\"{% if this.page.id == '4G' %}active{% endif %}\"><a href=\"4G.php\">4G & Internet</a>
<!--                                    <ul>
                            <li><a href=\"#\">Jumbo Data Pack</a></li>
                            <li><a href=\"#\">Double Data</a></li>
                            <li><a href=\"#\">First recharge pack</a></li>
                        </ul>-->
                    </li>
                    <!-- <li><a href=\"product.php\">Products</a></li> -->
                    <li class=\"{% if this.page.id == 'product' %}active{% endif %}\"><a href=\"{{ 'product'|page }}\">Products</a></li>
                    <li class=\"{% if this.page.id == 'offer' %}active{% endif %}\"><a href=\"{{ 'offer'|page }}\">Offer</a></li>
                    <li class=\"{% if this.page.id == 'international_call' %}active{% endif %}\"><a href=\"{{ 'international_call'|page }}\">International Call</a></li>
                    <li class=\"{% if this.page.id == 'services' %}active{% endif %}\"><a href=\"{{ 'services'|page }}\">Services</a></li>
                    <!-- <li><a href=\"#\">Offer</a></li>
                    <li><a href=\"#\">International Call</a></li>
                    <li><a href=\"#\">Services</a></li> -->
                </ul>
            </nav>
        </div>
    </div>
</div>", "C:\\xampp\\htdocs\\smart-cell/themes/smart/partials/site/header.htm", "");
    }
}
