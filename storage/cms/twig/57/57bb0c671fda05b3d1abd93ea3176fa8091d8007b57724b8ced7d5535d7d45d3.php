<?php

/* C:\xampp\htdocs\smart-cell/themes/smart/partials/site/footer.htm */
class __TwigTemplate_015e68a17b7d9d9b0c3965242f39ba632d4ee008a6f9aa4a46457cb104fbb847 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo "<!-- <div id=\"footer\">
    <div class=\"container\">
        <hr />
        <p class=\"muted credit\">&copy; 2013 - ";
        // line 4
        echo twig_escape_filter($this->env, twig_date_format_filter($this->env, "now", "Y"), "html", null, true);
        echo " Alexey Bobkov &amp; Samuel Georges.</p>
    </div>
</div> -->

<div class=\"top-footer\">
    <div class=\"custom-container add-padding-top-bottom\">
        <div class=\"row top-footer-eq-container\">
            <div class=\"col-sm-3 top-footer-eq-item\">
                <ul class=\"footer-links\">
                    <li><a href=\"#\">About Smart</a></li>
                    <li><a href=\"#\">Cell</a></li>
                    <li><a href=\"#\">Profile</a></li>
                    <li><a href=\"#\">CSR</a></li>
                    <li><a href=\"#\">Event</a></li>
                    <li><a href=\"#\">In news</a></li>
                    <li><a href=\"#\">Career</a></li>
                    <li><a href=\"#\">Coverage</a></li>
                    <li><a href=\"#\">Contact</a></li>
                </ul>
            </div>
            <div class=\"col-sm-3 top-footer-eq-item\">
                <ul class=\"footer-links\">
                    <li><a href=\"#\">4G internet</a></li>
                    <li><a href=\"#\">voice</a></li>
                    <li><a href=\"#\">data</a></li>
                    <li><a href=\"#\">unlimited talk time</a></li>
                    <li><a href=\"#\">international call</a></li>
                    <li><a href=\"#\">special offers</a></li>
                    <li><a href=\"#\">special tariff pack</a></li>
                    <li><a href=\"#\">sms</a></li>
                    <li><a href=\"#\">crbt</a></li>
                    <li><a href=\"#\">recharge cards</a></li>
                    <li><a href=\"#\">miss call alert</a></li>
                </ul>
            </div>
            <div class=\"col-sm-3 top-footer-eq-item\">
                <ul class=\"footer-links\">
                    <li><a href=\"#\">Online chat call</a></li>
                    <li><a href=\"#\">center contact</a></li>
                    <li><a href=\"#\">address smart</a></li>
                    <li><a href=\"#\">cell centers</a></li>
                </ul>
            </div>
            <div class=\"col-sm-3 top-footer-eq-item\">
                <img src=\"img/footer-logo.png\">
                <div class=\"footer-social\">
                    <ul>
                        <li><a href=\"#\"><img src=\"img/social/tw.png\"></a></li>
                        <li><a href=\"#\"><img src=\"img/social/in.png\"></a></li>
                        <li><a href=\"#\"><img src=\"img/social/yt.png\"></a></li>
                        <li><a href=\"#\"><img src=\"img/social/fb.png\"></a></li>
                        <li><a href=\"#\"><img src=\"img/social/go.png\"></a></li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
</div>

<div class=\"bottom-footer\">
    <div class=\"custom-container\">
        <ul>
            <li><a href=\"#\">Terms & Condition</a></li>
            <li><a href=\"#\">Privacy</a></li>
            <li><a href=\"#\">Legal Notice</a></li>
        </ul>
    </div>
</div>";
    }

    public function getTemplateName()
    {
        return "C:\\xampp\\htdocs\\smart-cell/themes/smart/partials/site/footer.htm";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  24 => 4,  19 => 1,);
    }

    public function getSourceContext()
    {
        return new Twig_Source("<!-- <div id=\"footer\">
    <div class=\"container\">
        <hr />
        <p class=\"muted credit\">&copy; 2013 - {{ \"now\"|date(\"Y\") }} Alexey Bobkov &amp; Samuel Georges.</p>
    </div>
</div> -->

<div class=\"top-footer\">
    <div class=\"custom-container add-padding-top-bottom\">
        <div class=\"row top-footer-eq-container\">
            <div class=\"col-sm-3 top-footer-eq-item\">
                <ul class=\"footer-links\">
                    <li><a href=\"#\">About Smart</a></li>
                    <li><a href=\"#\">Cell</a></li>
                    <li><a href=\"#\">Profile</a></li>
                    <li><a href=\"#\">CSR</a></li>
                    <li><a href=\"#\">Event</a></li>
                    <li><a href=\"#\">In news</a></li>
                    <li><a href=\"#\">Career</a></li>
                    <li><a href=\"#\">Coverage</a></li>
                    <li><a href=\"#\">Contact</a></li>
                </ul>
            </div>
            <div class=\"col-sm-3 top-footer-eq-item\">
                <ul class=\"footer-links\">
                    <li><a href=\"#\">4G internet</a></li>
                    <li><a href=\"#\">voice</a></li>
                    <li><a href=\"#\">data</a></li>
                    <li><a href=\"#\">unlimited talk time</a></li>
                    <li><a href=\"#\">international call</a></li>
                    <li><a href=\"#\">special offers</a></li>
                    <li><a href=\"#\">special tariff pack</a></li>
                    <li><a href=\"#\">sms</a></li>
                    <li><a href=\"#\">crbt</a></li>
                    <li><a href=\"#\">recharge cards</a></li>
                    <li><a href=\"#\">miss call alert</a></li>
                </ul>
            </div>
            <div class=\"col-sm-3 top-footer-eq-item\">
                <ul class=\"footer-links\">
                    <li><a href=\"#\">Online chat call</a></li>
                    <li><a href=\"#\">center contact</a></li>
                    <li><a href=\"#\">address smart</a></li>
                    <li><a href=\"#\">cell centers</a></li>
                </ul>
            </div>
            <div class=\"col-sm-3 top-footer-eq-item\">
                <img src=\"img/footer-logo.png\">
                <div class=\"footer-social\">
                    <ul>
                        <li><a href=\"#\"><img src=\"img/social/tw.png\"></a></li>
                        <li><a href=\"#\"><img src=\"img/social/in.png\"></a></li>
                        <li><a href=\"#\"><img src=\"img/social/yt.png\"></a></li>
                        <li><a href=\"#\"><img src=\"img/social/fb.png\"></a></li>
                        <li><a href=\"#\"><img src=\"img/social/go.png\"></a></li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
</div>

<div class=\"bottom-footer\">
    <div class=\"custom-container\">
        <ul>
            <li><a href=\"#\">Terms & Condition</a></li>
            <li><a href=\"#\">Privacy</a></li>
            <li><a href=\"#\">Legal Notice</a></li>
        </ul>
    </div>
</div>", "C:\\xampp\\htdocs\\smart-cell/themes/smart/partials/site/footer.htm", "");
    }
}
