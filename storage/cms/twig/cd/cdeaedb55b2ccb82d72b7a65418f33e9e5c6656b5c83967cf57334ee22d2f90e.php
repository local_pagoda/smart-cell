<?php

/* C:\xampp\htdocs\smart-cell/themes/smart/layouts/default.htm */
class __TwigTemplate_f9cd85d816ea6c6020e3ad67dc1e83e8b54ee13a9af35905f4827622e00099fe extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo "<!DOCTYPE html>
<html>
    <head>
        <meta charset=\"utf-8\">
        <title>Smarttel - ";
        // line 5
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->getSourceContext(), twig_get_attribute($this->env, $this->getSourceContext(), ($context["this"] ?? null), "page", array()), "title", array()), "html", null, true);
        echo "</title>
        <meta name=\"description\" content=\"";
        // line 6
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->getSourceContext(), twig_get_attribute($this->env, $this->getSourceContext(), ($context["this"] ?? null), "page", array()), "meta_description", array()), "html", null, true);
        echo "\">
        <meta name=\"title\" content=\"";
        // line 7
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->getSourceContext(), twig_get_attribute($this->env, $this->getSourceContext(), ($context["this"] ?? null), "page", array()), "meta_title", array()), "html", null, true);
        echo "\">
        <meta name=\"author\" content=\"OctoberCMS\">
        <meta name=\"viewport\" content=\"width=device-width, initial-scale=1.0\">
        <meta name=\"generator\" content=\"OctoberCMS\">
        <link rel=\"icon\" type=\"image/png\" href=\"";
        // line 11
        echo $this->env->getExtension('Cms\Twig\Extension')->themeFilter("assets/images/site_logo.png");
        echo "\">
        <link href=\"";
        // line 12
        echo $this->env->getExtension('Cms\Twig\Extension')->themeFilter("assets/css/bowercomponent/slick.css");
        echo "\" rel=\"stylesheet\">
        <link href=\"";
        // line 13
        echo $this->env->getExtension('Cms\Twig\Extension')->themeFilter("assets/css/bowercomponent/slick-theme.css");
        echo "\" rel=\"stylesheet\">
        <link href=\"";
        // line 14
        echo $this->env->getExtension('Cms\Twig\Extension')->themeFilter("assets/css/bowercomponent/bootstrap.min.css");
        echo "\" rel=\"stylesheet\">
        <link href=\"";
        // line 15
        echo $this->env->getExtension('Cms\Twig\Extension')->themeFilter("assets/css/bowercomponent/jquery.mCustomScrollbar.min.css");
        echo "\" rel=\"stylesheet\">
        <link href=\"";
        // line 16
        echo $this->env->getExtension('Cms\Twig\Extension')->themeFilter("assets/css/basestyles.css");
        echo "\" rel=\"stylesheet\">

        <!--------------------- Scripts Starts ---------------------->
        <script type=\"text/javascript\" src=\"";
        // line 19
        echo $this->env->getExtension('Cms\Twig\Extension')->themeFilter("assets/js/jquery.min.js");
        echo "\"></script>
        <script type=\"text/javascript\" src=\"";
        // line 20
        echo $this->env->getExtension('Cms\Twig\Extension')->themeFilter("assets/js/bootstrap.min.js");
        echo "\"></script>

    </head>
    <body>
        <div id=\"wrapper\">
            <!-- Header -->
            <header id=\"header-wrapper\">
                ";
        // line 27
        $context['__cms_partial_params'] = [];
        echo $this->env->getExtension('Cms\Twig\Extension')->partialFunction("site/header"        , $context['__cms_partial_params']        , true        );
        unset($context['__cms_partial_params']);
        // line 28
        echo "            </header>
            <div id=\"content-wrapper\">

                <!-- Content -->
                <section id=\"layout-content\">
                    ";
        // line 33
        echo $this->env->getExtension('Cms\Twig\Extension')->pageFunction();
        // line 34
        echo "                </section>
            </div>
            <!--------------------------------------- Header Wrapper Ends --------------------------------------->
        <!-- Footer -->
        </div>
        <footer id=\"footer-wrapper\">
            ";
        // line 40
        $context['__cms_partial_params'] = [];
        echo $this->env->getExtension('Cms\Twig\Extension')->partialFunction("site/footer"        , $context['__cms_partial_params']        , true        );
        unset($context['__cms_partial_params']);
        // line 41
        echo "        </footer>

        <!-- Scripts -->
        <!-- <script src=\"";
        // line 44
        echo $this->env->getExtension('Cms\Twig\Extension')->themeFilter("assets/vendor/jquery.js");
        echo "\"></script>
        <script src=\"";
        // line 45
        echo $this->env->getExtension('Cms\Twig\Extension')->themeFilter("assets/vendor/bootstrap.js");
        echo "\"></script>
        <script src=\"";
        // line 46
        echo $this->env->getExtension('Cms\Twig\Extension')->themeFilter("assets/javascript/app.js");
        echo "\"></script> -->

        <script type=\"text/javascript\" src=\"";
        // line 48
        echo $this->env->getExtension('Cms\Twig\Extension')->themeFilter("assets/js/slick.min.js");
        echo "\"></script>
        <script type=\"text/javascript\" src=\"";
        // line 49
        echo $this->env->getExtension('Cms\Twig\Extension')->themeFilter("assets/js/jquery.mCustomScrollbar.concat.min.js");
        echo "\"></script>
        <script type=\"text/javascript\" src=\"";
        // line 50
        echo $this->env->getExtension('Cms\Twig\Extension')->themeFilter("assets/js/jquery.matchHeight-min.js");
        echo "\"></script>
        <script type=\"text/javascript\" src=\"";
        // line 51
        echo $this->env->getExtension('Cms\Twig\Extension')->themeFilter("assets/js/thescripts.js");
        echo "\"></script>
        ";
        // line 52
        echo '<script src="'. Request::getBasePath()
                .'/modules/system/assets/js/framework.js"></script>'.PHP_EOL;
        echo '<script src="'. Request::getBasePath()
                    .'/modules/system/assets/js/framework.extras.js"></script>'.PHP_EOL;
        echo '<link rel="stylesheet" property="stylesheet" href="'. Request::getBasePath()
                    .'/modules/system/assets/css/framework.extras.css">'.PHP_EOL;
        // line 53
        echo "        ";
        echo $this->env->getExtension('Cms\Twig\Extension')->assetsFunction('js');
        echo $this->env->getExtension('Cms\Twig\Extension')->displayBlock('scripts');
        // line 54
        echo "
    </body>
</html>";
    }

    public function getTemplateName()
    {
        return "C:\\xampp\\htdocs\\smart-cell/themes/smart/layouts/default.htm";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  150 => 54,  146 => 53,  139 => 52,  135 => 51,  131 => 50,  127 => 49,  123 => 48,  118 => 46,  114 => 45,  110 => 44,  105 => 41,  101 => 40,  93 => 34,  91 => 33,  84 => 28,  80 => 27,  70 => 20,  66 => 19,  60 => 16,  56 => 15,  52 => 14,  48 => 13,  44 => 12,  40 => 11,  33 => 7,  29 => 6,  25 => 5,  19 => 1,);
    }

    public function getSourceContext()
    {
        return new Twig_Source("<!DOCTYPE html>
<html>
    <head>
        <meta charset=\"utf-8\">
        <title>Smarttel - {{ this.page.title }}</title>
        <meta name=\"description\" content=\"{{ this.page.meta_description }}\">
        <meta name=\"title\" content=\"{{ this.page.meta_title }}\">
        <meta name=\"author\" content=\"OctoberCMS\">
        <meta name=\"viewport\" content=\"width=device-width, initial-scale=1.0\">
        <meta name=\"generator\" content=\"OctoberCMS\">
        <link rel=\"icon\" type=\"image/png\" href=\"{{ 'assets/images/site_logo.png'|theme }}\">
        <link href=\"{{ 'assets/css/bowercomponent/slick.css'|theme }}\" rel=\"stylesheet\">
        <link href=\"{{ 'assets/css/bowercomponent/slick-theme.css'|theme }}\" rel=\"stylesheet\">
        <link href=\"{{ 'assets/css/bowercomponent/bootstrap.min.css'|theme }}\" rel=\"stylesheet\">
        <link href=\"{{ 'assets/css/bowercomponent/jquery.mCustomScrollbar.min.css'|theme }}\" rel=\"stylesheet\">
        <link href=\"{{ 'assets/css/basestyles.css'|theme }}\" rel=\"stylesheet\">

        <!--------------------- Scripts Starts ---------------------->
        <script type=\"text/javascript\" src=\"{{ 'assets/js/jquery.min.js'|theme }}\"></script>
        <script type=\"text/javascript\" src=\"{{ 'assets/js/bootstrap.min.js'|theme }}\"></script>

    </head>
    <body>
        <div id=\"wrapper\">
            <!-- Header -->
            <header id=\"header-wrapper\">
                {% partial 'site/header' %}
            </header>
            <div id=\"content-wrapper\">

                <!-- Content -->
                <section id=\"layout-content\">
                    {% page %}
                </section>
            </div>
            <!--------------------------------------- Header Wrapper Ends --------------------------------------->
        <!-- Footer -->
        </div>
        <footer id=\"footer-wrapper\">
            {% partial 'site/footer' %}
        </footer>

        <!-- Scripts -->
        <!-- <script src=\"{{ 'assets/vendor/jquery.js'|theme }}\"></script>
        <script src=\"{{ 'assets/vendor/bootstrap.js'|theme }}\"></script>
        <script src=\"{{ 'assets/javascript/app.js'|theme }}\"></script> -->

        <script type=\"text/javascript\" src=\"{{ 'assets/js/slick.min.js'|theme }}\"></script>
        <script type=\"text/javascript\" src=\"{{ 'assets/js/jquery.mCustomScrollbar.concat.min.js'|theme }}\"></script>
        <script type=\"text/javascript\" src=\"{{ 'assets/js/jquery.matchHeight-min.js'|theme }}\"></script>
        <script type=\"text/javascript\" src=\"{{ 'assets/js/thescripts.js'|theme }}\"></script>
        {% framework extras %}
        {% scripts %}

    </body>
</html>", "C:\\xampp\\htdocs\\smart-cell/themes/smart/layouts/default.htm", "");
    }
}
