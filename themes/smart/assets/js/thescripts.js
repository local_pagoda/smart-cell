/**
 * Created by Sushant on 12/14/2017.
 */
$(document).ready(function () {
    sliderInit();
    matchHeight();
});


/*------------------------------- Functions Starts -------------------------------*/
function sliderInit() {
    $('.home-banner-slider').slick({
        arrows: false,
        dots: true,
        autoplay: false,
        speed: 500,
        slidesToShow: 1
    });
    $('.home-packs-slider').slick({
        arrows: false,
        dots: true,
        autoplay: false,
        speed: 500,
        slidesToShow: 4,
        slidesToScroll: 4
    });

    $('.blue-back-slider').slick({
        arrows: false,
        dots: false,
        autoplay: false,
        speed: 500,
        slidesToShow: 3,
        slidesToScroll: 3
    });
}

function matchHeight() {
    $('body').addClass('equal');
    var byRow = $('body').hasClass('equal');

    // apply matchHeight to each item container's items
    $('.top-footer-eq-container').each(function() {
        $(this).children('.top-footer-eq-item').matchHeight({
            byRow: byRow
        });
    });

    $('.fourg-tab-section .section-title ul').each(function() {
        $(this).children('.fourg-tab-section .section-title ul li').matchHeight({
            byRow: byRow
        });
    });

    // example of update callbacks (uncomment to test)
    $.fn.matchHeight._beforeUpdate = function(event, groups) {
        //var eventType = event ? event.type + ' event, ' : '';
        //console.log("beforeUpdate, " + eventType + groups.length + " groups");
    }

    $.fn.matchHeight._afterUpdate = function(event, groups) {
        //var eventType = event ? event.type + ' event, ' : '';
        //console.log("afterUpdate, " + eventType + groups.length + " groups");
    }
}
/*-------------------------------- Functions Ends --------------------------------*/
